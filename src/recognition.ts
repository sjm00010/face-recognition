import { readdir } from 'fs/promises';
import { join } from 'path';
import * as canvas from 'canvas';
import * as faceapi from '@vladmandic/face-api';
import { imagesPath } from './init';

async function getFolders(path: string): Promise<string[]> {
    return (await readdir(path, { withFileTypes: true })).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);
}

async function getLabeledDescriptors(imagesPath: string) {
    const labels = await getFolders(imagesPath);
    const descriptors = await Promise.all(
        labels.map(async label => {
            const descriptors = [];
            const labelPath = join(imagesPath, label);
            const images = readdir(labelPath);
            for (const imagePath of await images) {
                const img: any = await canvas.loadImage(join(labelPath, imagePath));
                const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();

                if (detections) descriptors.push(detections.descriptor);
            }
            if (descriptors.length == 0) {
                console.log(`No descriptors found for ${label}`);
                return null;
            }

            console.log(`Found ${descriptors.length} descriptors for ${label}`);
            return new faceapi.LabeledFaceDescriptors(label, descriptors);
        })

    );

    return descriptors.filter(d => d != null);
}

const getFaceMatcher = async (imagesPath: string): Promise<faceapi.FaceMatcher> => {
    const labeledDescriptors = await getLabeledDescriptors(imagesPath);
    if (labeledDescriptors.length === 0) throw new Error(`Not enough labels, got ${labeledDescriptors.length}`);
    return new faceapi.FaceMatcher(labeledDescriptors, 0.6);
};

interface IRecognize {
    label: string;
    distance: number;
    x: number;
    y: number;
    width: number;
    height: number;
}

export const recognize = async (buffer: Buffer, faceMatcher: faceapi.FaceMatcher): Promise<IRecognize> => {
    if (faceMatcher == undefined) upadteMatcher();
    const img: any = await canvas.loadImage(buffer, { encoding: 'base64' });
    const detection = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();

    if(detection == undefined) return { label: '', distance: NaN, x: NaN, y: NaN, width: NaN, height: NaN };

    const { label, distance } = faceMatcher.findBestMatch(detection.descriptor);
    const { x, y, width, height } = detection.detection.box;
    return { label, distance, x, y, width, height };

}

export let faceMatcher: faceapi.FaceMatcher;

export const upadteMatcher = async () => faceMatcher = await getFaceMatcher(imagesPath);

export const containsFace = async (buffer: Buffer): Promise<boolean> => {
    const img: any = await canvas.loadImage(buffer, { encoding: 'base64' });
    const detections = await faceapi.detectAllFaces(img).withFaceLandmarks().withFaceDescriptors();
    return detections.length > 0;
}