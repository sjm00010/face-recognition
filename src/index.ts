import { setupEnv, loadModels, modelsPath } from './init';
import { upadteMatcher } from './recognition';
import express, { Express } from 'express';
import router from './routes';
import session from 'express-session';

async function server() {
    setupEnv();
    loadModels(modelsPath)
        .then(() => console.log('Models loaded'))
        .then(() => upadteMatcher())
        .then(() => console.log('Ready to recognize faces'));

    const app: Express = express();
    const port = 3000;

    app.use(express.json({ limit: '50mb' }));
    app.use(express.urlencoded({ extended: true }));
    app.use(session({ secret: process.env.sessionSecret ?? 'faceRecognition', resave: true, saveUninitialized: true }));

    app.set('view engine', 'ejs');

    app.set('views', __dirname);

    app.use(express.static(__dirname + '/views'));

    app.use(router);

    app.listen(port, () => {
        console.log(`Server listening at http://localhost:${port}`)
    });
}

server();