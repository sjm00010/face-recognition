import express, { Request, Response, Router } from 'express';
import { writeFile } from 'fs/promises';
import { existsSync, mkdirSync } from 'fs';
import { faceMatcher, upadteMatcher, recognize, containsFace } from './recognition';
import { imagesPath } from './init';
import { join } from 'path';

declare module 'express-session' {
    export interface SessionData {
        username: string;
        errorMsg: string;
    }
}

const router: Router = express.Router();

router.post("/train", async (req: Request, res: Response) => {
    const { label, data } = req.body;

    if (!(await containsFace(data))) {
        res.status(404).send({ errorMsg: "No face detected" });
        return;
    }

    const labelPath = join(imagesPath, label);
    const img = data.replace(/^data:image\/(png|jpeg|jpg);base64,/, '');
    const timestamp = Date.now();
    if (!existsSync(labelPath)) mkdirSync(labelPath);

    await writeFile(join(labelPath, `${timestamp}.jpg`), img, {
        encoding: 'base64'
    });
    await upadteMatcher();

    console.log('Ready to recognize faces');

    res.sendStatus(200);
});

router.post("/recognize", async (req: Request, res: Response) => {
    const { data } = req.body;
    const img: Buffer = data.replace(/^data:image\/(png|jpeg|jpg);base64,/, '');
    const result = await recognize(data, faceMatcher);

    if (result.label.length == 0 || result.label == 'unknown') {
        res.status(401).send({ errorMsg: "No face detected" });        
        return;
    }

    req.session.username = result.label;
    res.redirect(`welcome`);
});

router.get("/", (req: Request, res: Response) => {    
    const msg = req.session.errorMsg ?? '';
    req.session.errorMsg = undefined;
    res.render('views/login', { errorMsg: msg});
});

router.get("/welcome", (req: Request, res: Response) => {
    if (req.session.username == undefined) {
        req.session.errorMsg = "You must login first";
        res.redirect('/');
        return;
    }

    res.render('views/welcome', { username: req.session.username });
});

router.post("/logout", (req: Request, res: Response) => {
    req.session.username = undefined;
    res.redirect('/');
});



export default router;