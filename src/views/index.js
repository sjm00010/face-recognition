const video = document.getElementById('videoSource');
const canvas = document.getElementById('imageBuffer');
const context = canvas.getContext('2d');

async function startCamera() {
    return await navigator.mediaDevices.getUserMedia({
        video: true
    }).then(function (stream) {
        video.srcObject = stream;
        return video.play();
    }).then(function () {
        video.setAttribute('width', video.videoWidth);
        video.setAttribute('height', video.videoHeight);
    }).catch(function (error) {
        console.error('Error starting the camera: ' + error);
    });
}

function stopCamera() {
    const stream = video.srcObject;
    stream.getTracks().forEach(function (track) {
        track.stop();
    });
}

function sendBuffer() {
    document.getElementById('trainModel').disabled = true;
    context.drawImage(video, 0, 0);
    const data = canvas.toDataURL('image/jpeg');
    const label = document.getElementById('trainLabel').value;
    const dataToSend = {
        data,
        label
    };
    fetch('/train', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    }).then(async function (response) {
        if (!response.ok) {
            throw await response.json();
        }
        console.log('Response status:' + response.status);
    }).catch(function ({ errorMsg }) {
        alert('Error training data: ' + errorMsg);
    }).finally(function () {
        document.getElementById('trainModel').disabled = false;
    });
}

async function detectUser() {
    document.getElementById('detectUser').disabled = true;
    context.drawImage(video, 0, 0);
    const data = canvas.toDataURL('image/jpeg');
    const dataToSend = {
        data
    };
    return await fetch('/recognize', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    }).then(function (response) {
        if (response.status == 401) {
            document.getElementById('loginError').innerHTML = 'Unkonwn user';
            setTimeout(() => document.getElementById('loginError').innerHTML = '', 2000);
        }
        
        if (response.redirected) {
            window.location.href = response.url;
        }
    }).catch(function (error) {
        console.error('Error detecting user: ' + error);
    }).finally(function () {
        document.getElementById('detectUser').disabled = false;
    });
}

startCamera().then(() => {
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
});

document.getElementById('initCamera').addEventListener('click', startCamera);
document.getElementById('stopCamera').addEventListener('click', stopCamera);
document.getElementById('trainModel').addEventListener('click', sendBuffer);
document.getElementById('detectUser').addEventListener('click', detectUser);
